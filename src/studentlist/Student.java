package studentlist;

/**
 * This class represents students in our application
 *
 * @author Paul Bonenfant
 */
public class Student {

    private String Program;

    /**
     * Get the value of Program
     *
     * @return the value of Program
     */
    public String getProgram() {
        return Program;
    }

    /**
     * Set the value of Program
     *
     * @param Program new value of Program
     */
    public void setProgram(String Program) {
        this.Program = Program;
    }

    
    private String name;

    public Student(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    

}

